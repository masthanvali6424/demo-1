package com.arun.springbasics.javaconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Scope(scopeName = "singleton")
@Component
public class Dance {
	
	@Value("michael jackson")
	String dancerName;
	
	@Autowired
	Music music;
	
	public Dance() {
		System.out.println(this.getClass().getSimpleName());
	}

	public String getDancerName() {
		return dancerName;
	}

	public void setDancerName(String dancerName) {
		this.dancerName = dancerName;
	}

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}

	@Override
	public String toString() {
		return "Dance [dancerName=" + dancerName + ", music=" + music + "]";
	}
	
	
}
